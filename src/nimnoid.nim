import game/game
import ui/ui as uilib
import sdl2
# import nimprof

var state = newGameState()

var ui = newUserInterface()

init ui;

var totalTicks : uint32 = 0;
var counter = 0

while true:
  let (cmd, ticks) = read ui;
  var direction = 0;

  var startTime = getTicks();
  case cmd:
  of UiCommand.Quit: break
  of UiCommand.MoveLeft: direction = -1
  of UiCommand.MoveRight: direction = 1
  of UiCommand.Launch: state.launch()
  else: discard
  state.tick(direction, ticks)
  ui.draw state;

  totalTicks = totalTicks + (getTicks() - startTime)
  inc(counter)
  if counter > 1000:
    echo 1000.0 / (totalTicks.float / counter.float), " fps"
    totalTicks = 0;
    counter = 0;
