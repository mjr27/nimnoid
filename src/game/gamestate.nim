import basic2d, math
import sequtils
import game_math, game_collidables, game_types, game_objects, game_collisioncheck

import ../config


proc launch*(self: GameState) =
  self.ballStuck = false
  self.ballSpeed = vector2d(-1, -1)
  discard tryNormalize self.ballSpeed
  self.ballSpeed *= DEFAULT_BALL_SPEED

  self.ball.position = point2d(self.paddleLeftTop.x + self.paddleSize.x / 4, self.paddleLeftTop.y - self.ballRadius)


  self.ballSpeed = vector2d(10, 0)
  self.paddleSpeed = -20
  self.ball.position = point2d(1.5, self.paddleLeftTop.y + 1)

  const brickSize = vector2d(20.0, 10.0);
  const brickSpace = vector2d(0.1, 0.1)

  var y = brickSpace.y
  var i = 1;
  while y < self.size.y / 2:
    var x = brickSpace.x
    while x < self.size.x - brickSize.x:
      self.bricks.add(newBrick(i, point2d(x, y), brickSize))
      inc(i)
      x += brickSize.x + brickSpace.x
    y += brickSize.y + brickSpace.y

proc tick * (self:GameState, direction: int, time: float) =
  var dT : float = time - self.lastTick
  self.lastTick = time

  if direction < 0:
    self.paddleSpeed -= PADDLE_ACCEL * dT
  elif direction > 0:
    self.paddleSpeed += PADDLE_ACCEL * dT

  if self.paddleSpeed > PADDLE_MAX_SPEED:
    self.paddleSpeed = PADDLE_MAX_SPEED
  if self.paddleSpeed < -PADDLE_MAX_SPEED:
    self.paddleSpeed = -PADDLE_MAX_SPEED


  if direction == 0 or (direction < 0 and self.paddleSpeed > 0) or (direction > 0 and self.paddleSpeed < 0):
    if self.paddleSpeed > 0:
      self.paddleSpeed = max(0, self.paddleSpeed - PADDLE_BREAK * dT)
    elif self.paddleSpeed < 0:
      self.paddleSpeed = min(0, self.paddleSpeed + PADDLE_BREAK * dT)

  if self.paddleLeftTop.x < 0.0:
    self.paddleLeftTop.x = 0
    self.paddleSpeed = 0
  if self.paddleLeftTop.x > self.size.x - self.paddleSize.x:
    self.paddleLeftTop.x = self.size.x - self.paddleSize.x
    self.paddleSpeed = 0

  if not self.ballStuck:
    var remaining = dT
    while self.checkPaddleCollisions(remaining) > 0:
      discard
    self.paddleLeftTop.x += self.paddleSpeed * dT


    remaining = dT
    var found = false
    while remaining > 0:
      var (rem, objectId) = self.checkCollisions(remaining)
      remaining = rem;
      if objectId != 0:
        for r in self.bricks.mitems:
          if r.id == objectId:
            r.broken = true
            found = true
