import basic2d, math
import
  ./game_types,
  ./game_collidables

proc newBrick* (id: int, position: Point2d, size: Vector2d): Brick =
  result.id = id
  result.position = position
  result.size = size

iterator collidables* (brick: Brick, radius: float32): CollidableObject =
  let endPosition = brick.position + brick.size;
  let corner1 = brick.position
  let corner2 = point2d(endPosition.x, brick.position.y)
  let corner3 = point2d(brick.position.x, endPosition.y)
  let corner4 = endPosition

  yield newCollidableLine(corner1 - vector2d(0, radius), corner2 - vector2d(0, radius), brick.id)
  yield newCollidableLine(corner1 - vector2d(radius, 0), corner3 - vector2d(radius, 0), brick.id)
  yield newCollidableLine(corner4 + vector2d(radius, 0), corner2 + vector2d(radius, 0), brick.id)
  yield newCollidableLine(corner4 + vector2d(0, radius), corner3 + vector2d(0, radius), brick.id)

  yield newCollidableArc(corner1, radius, math.PI * 3/ 2, math.PI * 2, brick.id)
  yield newCollidableArc(corner2, radius, 0, math.PI / 2, brick.id)
  yield newCollidableArc(corner3, radius, math.PI, math.PI * 3 / 2, brick.id)
  yield newCollidableArc(corner4, radius, math.PI / 2, math.PI, brick.id)
