import basic2d
import
  ./game_types,
  ./game_math



proc collides*(self: CollidableObject, point: Point2d, speed: Vector2d, collisionPoint: var Point2d): bool {.noSideEffect.} =
  case self.kind:
  of Line:
    return collidesPointWithLine(point, self.p1, self.p2, speed, collisionPoint)
  of Arc:
    return collidesPointWithCircle(point, self.center, self.radius, speed, collisionPoint)
  else:
    debugEcho  "collide is not implemented for", self.kind

proc reflect*(self: CollidableObject, collisionVector: Vector2d, collisionPoint: Point2d): Vector2d  {.noSideEffect.} =
  case self.kind:
    of Line:
      return reflectFromLine(self.p1 - self.p2, collisionVector, collisionPoint)
    of Arc:
      return reflectFromCircle(self.center, collisionVector, collisionPoint)
    else:
      debugEcho "reflect is not implemented for ", self.kind

proc collides2*(self: CollidableObject, point: Point2d, speed: Vector2d, pointSpeed: Vector2d, collisionPoint: var Point2d): bool {.noSideEffect.} =
  case self.kind:
  of Line:
    if not collidesPointWithLine(point, self.p1, self.p2, pointSpeed - speed, collisionPoint):
      return false
  of Arc:
    if not collidesPointWithCircle(point, self.center, self.radius, pointSpeed - speed, collisionPoint):
      return false
  else:
    debugEcho  "collides2 is not implemented for ", self.kind
    return false
  var totalLen = len(pointSpeed - speed)
  var myLen = len(point - collisionPoint)
  collisionPoint += speed * myLen/totalLen
  return true

proc reflect2*(self: CollidableObject, speed: Vector2d, pointSpeed: Vector2d, collisionPoint: Point2d): Vector2d  {.noSideEffect.} =
  case self.kind:
    of Line:
      return reflectFromLine(self.p1 - self.p2, pointSpeed, collisionPoint) + speed
    of Arc:
      return reflectFromCircle(self.center, pointSpeed, collisionPoint) + speed
    else:
      debugEcho "reflect is not implemented for ", self.kind
