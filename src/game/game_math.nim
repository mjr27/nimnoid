import math, basic2d


proc reflectFromLine*(lineVector: Vector2d, vector: Vector2d, collisionPoint: Point2d) : Vector2d {.noSideEffect.} =
  result = vector
  result.mirror(lineVector)

proc reflectFromCircle*(center: Point2d, vector: Vector2d, collisionPoint: Point2d) : Vector2d {.noSideEffect.} =
  var mirrorVector = collisionPoint - center
  rotate90(mirrorVector)
  result = vector
  result.mirror(mirrorVector)


proc collidesPointWithLine* (point: Point2d,
      linePoint1, linePoint2: Point2d,
      direction: Vector2d,
      collisionPoint: var Point2d) : bool {.noSideEffect.} =
  let end2 = point + direction
  let vector = linePoint2 - linePoint1

  # считаем уравнения прямых проходящих через отрезки
  let a1 = -vector.y
  let b1 = +vector.x;
  let d1 = -(a1 * linePoint1.x + b1*linePoint1.y)

  let a2 = -direction.y
  let b2 = +direction.x
  let d2 = -(a2 * point.x + b2 * point.y)

  # подставляем концы отрезков, для выяснения в каких полуплоскотях они
  var seg1_line2_start = a2 * linePoint1.x + b2 * linePoint1.y + d2
  var seg1_line2_end = a2 * linePoint2.x + b2 * linePoint2.y + d2

  var seg2_line1_start = a1*point.x + b1*point.y + d1
  var seg2_line1_end = a1*end2.x + b1*end2.y + d1

  # если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
  if (seg1_line2_start * seg1_line2_end > 0 or seg2_line1_start * seg2_line1_end >= 0):
    return false

  var u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
  collisionPoint = linePoint1 + u * vector;
  return true

proc solveSquare* (a, b, c: float): tuple[root1: float, root2: float] =
  result.root1 = NaN
  result.root2 = NaN
  let D = b * b - 4 * a * c;
  if D < 0:
    return
  let sqrtD = sqrt(D)
  result.root1 = (-b + sqrtD) / (2 * a)
  result.root2 = (-b - sqrtD) / (2 * a)

proc collidesPointWithCircle* (point: Point2d,
  center: Point2d,
  radius: float,
  direction: Vector2d,
  collisionPoint: var Point2d) : bool  =


  var centerVector = center - point;
  let centerVectorLen = len(centerVector)
  var centerVectorNorm = centerVector
  var speedVectorNorm = direction
  let speedVectorLen = len direction

  if speedVectorLen + radius < centerVectorLen:
    return false

  discard tryNormalize centerVectorNorm
  discard tryNormalize speedVectorNorm

  let cosA = centerVectorNorm.dot(speedVectorNorm)

  let b = -2 * centerVectorLen * cosA
  let c = centerVectorLen * centerVectorLen - radius * radius

  let res = solveSquare(1.0f, b, c)

  if res.root1.classify == fcNaN:
    return false
  if res.root1 >= 0 and res.root1 < res.root2 and res.root1 <= speedVectorLen:
    collisionPoint = point + speedVectorNorm * res.root1
    return true
  if res.root2 >= 0 and res.root2 <= speedVectorLen:
    collisionPoint = point + speedVectorNorm * res.root2
    return true
  return false
