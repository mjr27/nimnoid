import basic2d, ../config

type
  GameState * = ref object
    size*: Vector2d

    paddleSize* : Vector2d
    paddleLeftTop* : Point2d
    paddleSpeed*: float32

    ball*: CollidableObject
    ballRadius*: float
    ballSpeed*: Vector2d

    bricks*: seq[Brick]
    removedBricks: seq[Brick]

    ballStuck*: bool
    lastTick*: float32

  Brick* = object
    id*: int
    broken*: bool
    position*: Point2d
    size*: Vector2d

  ColliderKind * = enum
    Point,
    Line,
    Arc
  CollidableObject * = object
    objectId*: int
    case kind * : ColliderKind
    of Point:
      position*: Point2d
    of Line:
      p1*, p2*: Point2d
    of Arc:
      center*: Point2d
      startRad*, endRad*, radius*: float

proc newCollidableLine* (p1, p2: Point2d, objectId: int = 0) : CollidableObject =
  result.objectId = objectId
  result.kind = Line
  result.p1 = p1
  result.p2 = p2

proc newCollidablePoint* (position: Point2d) : CollidableObject =
  result.kind = Point
  result.position = position

proc newCollidableArc* (center: Point2d, radius: float, startRad: float, endRad: float, objectId: int = 0) : CollidableObject =
  result.kind = Arc
  result.center = center
  result.radius = radius
  result.startRad = startRad
  result.endRad = endRad
  result.objectId = objectId

proc newGameState* () : GameState =
  new(result)
  result.size = vector2d(70.0, 31.0)
  result.paddleSize = vector2d(10, 4)
  result.paddleLeftTop = point2d(
    result.size.x / 2 - result.paddleSize.x / 2,
    result.size.y - result.paddleSize.y - PADDLE_BOTTOM_MARGIN
  )
  result.paddleSpeed = 0
  result.ballStuck = true
  result.ballRadius = 1.0f
  result.bricks = newSeq[Brick]()
  result.removedBricks = newSeq[Brick]()

  result.ball = newCollidablePoint(point2d(0, 0))
