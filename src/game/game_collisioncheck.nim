import basic2d, math
import
  ./game_types, ./game_objects, ./game_collidables

iterator collideRectangle(p1: Point2d, p2: Point2d, radius: float, objectId: int = 0): CollidableObject =
  let corner1 = point2d(p2.x, p1.y)
  let corner2 = point2d(p1.x, p2.y)
  yield newCollidableLine(p1, corner1, objectId)
  yield newCollidableLine(p1, corner2, objectId)
  yield newCollidableLine(p2, corner1, objectId)
  yield newCollidableLine(p2, corner2, objectId)

iterator paddleCollidables*(self: GameState): CollidableObject =
  yield newCollidableLine(
    point2d(self.paddleLeftTop.x, self.paddleLeftTop.y - self.ballRadius),
    point2d(self.paddleLeftTop.x + self.paddleSize.x, self.paddleLeftTop.y - self.ballRadius))

  yield newCollidableLine(
    point2d(self.paddleLeftTop.x, self.paddleLeftTop.y + self.paddleSize.y - self.ballRadius),
    point2d(self.paddleLeftTop.x + self.paddleSize.x, self.paddleLeftTop.y + self.paddleSize.y - self.ballRadius))

  yield newCollidableArc(
    self.paddleLeftTop + vector2d(0, self.paddleSize.y / 2),
    self.paddleSize.y / 2 + self.ballRadius,
    math.PI,
    math.PI * 2)

  yield newCollidableArc(
    self.paddleLeftTop + vector2d(self.paddleSize.x, self.paddleSize.y / 2),
    self.paddleSize.y / 2 + self.ballRadius,
    0,
    math.PI)

iterator collidableBounds*(self: GameState): CollidableObject =
  ## borders
  for p in collideRectangle(point2d(self.ballRadius, self.ballRadius), point2d(-self.ballRadius, -self.ballRadius) + self.size, self.ballRadius):
    yield p

  for p in self.paddleCollidables:
    yield p

  for brick in self.bricks:
    if brick.broken:
      continue
    for p in brick.collidables(self.ballRadius):
      yield p


proc checkCollisions*(self: GameState, remaining: float) : tuple[remaining: float, objectId: int] =
  var collisionPoint: Point2d

  var reflectionPoint: Point2d
  var minDistance = Inf
  var collidedObject : CollidableObject;

  proc addPoint(obj:CollidableObject, point: Point2d) =
    let distance = len(point - self.ball.position) / len(self.ballSpeed)
    if distance < minDistance:
      reflectionPoint = point
      minDistance = distance
      collidedObject = obj

  for obj in self.collidableBounds:
    if obj.collides(
      self.ball.position,
      self.ballSpeed * remaining,
      collisionPoint
    ):
      addPoint(obj, collisionPoint)

  if minDistance != Inf:
    let reflectionVector = collidedObject.reflect(self.ballSpeed, reflectionPoint)
    self.ball.position = reflectionPoint + reflectionVector * 1e-6
    self.ballSpeed = reflectionVector
    result.remaining = remaining * (1 - minDistance) - 1e-6
    result.objectId = collidedObject.objectId
  else:
    self.ball.position = self.ball.position + self.ballSpeed * remaining;
    result.remaining = 0

proc checkPaddleCollisions*(self: GameState, remaining: float) : float =
  var collisionPoint: Point2d
  var reflectionPoint: Point2d
  var minDistance = Inf
  var collidedObject : CollidableObject;
  let paddleSpeed = vector2d(self.paddleSpeed, 0)


  for obj in self.paddleCollidables:
    if obj.collides2(
      self.ball.position,
      paddleSpeed,
      self.ballSpeed * remaining,
      collisionPoint
    ):
      let distance = len(collisionPoint - self.ball.position) / len(self.ballSpeed)
      if distance < minDistance:
        reflectionPoint = collisionPoint
        minDistance = distance
        collidedObject = obj
  if minDistance != Inf:
    var reflectionVector = collidedObject.reflect2(paddleSpeed, self.ballSpeed, reflectionPoint)
    self.ballSpeed = reflectionVector
    self.ball.position = reflectionPoint +  remaining * (1 - minDistance) * reflectionVector
    return 1 - minDistance
  else:
    return 0
