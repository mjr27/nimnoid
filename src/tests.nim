import unittest
import math
import basic2d
import game/game_math
import game/game_types
import game/game_collidables

suite "collisions":
  var collisionPoint: Point2d
  var line: CollidableObject;
  var pt: Point2d
  test "collides horizontal":
    line = newCollidableLine(point2d(0, 0), point2d(0, 10))
    require(line.collides(point2d(1, 0), vector2d(-2, 2), collisionPoint))
    require(collisionPoint == point2d(0, 1))

  test "collides vertical":
    line = newCollidableLine(point2d(0, 0), point2d(10, 0))
    require(line.collides(point2d(1, 1), vector2d(2, -2), collisionPoint))
    require(collisionPoint == point2d(2, 0))

  test "collides angle":
    pt = point2d(1, 0)
    line = newCollidableLine(point2d(0, 0), point2d(5, 5))
    require(line.collides(pt, vector2d(-2, 2), collisionPoint))
    require(collisionPoint == point2d(0.5, 0.5))

    line = newCollidableLine(point2d(0, 0), point2d(4, 2))
    require(line.collides(pt, vector2d(-2, 2), collisionPoint))
    require(not line.collides(point2d(1, 0), vector2d(-0.25, 0.25), collisionPoint))

  test "collides at border cases":
    pt = point2d(1, 0)
    line = newCollidableLine(point2d(0, 0), point2d(0.5, 0.5))
    require(line.collides(pt, vector2d(-2.3, 2.3), collisionPoint))
    require(not line.collides(pt, vector2d(-2.3, 2.31), collisionPoint))

  test "do not collide at start point":
    pt = point2d(0, 0)
    line = newCollidableLine(point2d(0, 0), point2d(0.5, 0.5))
    require(not line.collides(pt, vector2d(1, 0), collisionPoint))

suite "reflection":
  test "reflect vertical":
    #  | /
    #  | \
    let co = newCollidableLine(point2d(0, 0), point2d(0, 10))
    let collisionPoint = point2d(0, 5)
    require(co.reflect(vector2d(-1, -1), collisionPoint) == vector2d(1, -1))
    require(co.reflect(vector2d(+1, +1), collisionPoint) == vector2d(-1, 1))

  test "reflect horizontal":
    #
    #  \/
    #_______
    let co = newCollidableLine(point2d(0, 0), point2d(10, 0))
    let collisionPoint = point2d(5, 0)
    require(co.reflect(vector2d(-1, -1), collisionPoint) == vector2d(-1, 1))
    require(co.reflect(vector2d(1, 1), collisionPoint) == vector2d(1, -1))

  test "collides angle":
    #   /
    #  / - - -
    # /|
    #/ |
    let co = newCollidableLine(point2d(0, 0), point2d(5, 5))
    let collisionPoint = point2d(2, 2)

    require(co.reflect(vector2d(-2, 2), collisionPoint) =~ vector2d(2, -2))

    require(co.reflect(vector2d(0, 1), collisionPoint) == vector2d(1, 0))
    require(co.reflect(vector2d(-1, 0), collisionPoint) == vector2d(0, -1))
    require(co.reflect(vector2d(0, -1), collisionPoint) == vector2d(-1, 0))
    require(co.reflect(vector2d(1, 0), collisionPoint) == vector2d(0, 1))

  test "collides angle reverse":
    let co = newCollidableLine(point2d(5, 5), point2d(0, 0))
    let collisionPoint = point2d(2, 2)
    require(co.reflect(vector2d(-2, 2), collisionPoint) =~ vector2d(2, -2))

    require(co.reflect(vector2d(0, 1), collisionPoint) == vector2d(1, 0))
    require(co.reflect(vector2d(-1, 0), collisionPoint) == vector2d(0, -1))
    require(co.reflect(vector2d(0, -1), collisionPoint) == vector2d(-1, 0))
    require(co.reflect(vector2d(1, 0), collisionPoint) == vector2d(0, 1))

suite "arc collisions":
  var collisionPoint: Point2d
  var arc: CollidableObject = newCollidableArc(point2d(0, 0), 1, 0, math.PI / 2)
  var pt: Point2d

  test "collide arc at angle":
    pt = point2d(2, 1);
    require(arc.collides(pt, vector2d(-1.1, -1.1), collisionPoint))
    require(collisionPoint =~ point2d(1, 0))

  test "collide arc horizontal":
    pt = point2d(2, 0);
    require(arc.collides(pt, vector2d(-1.1, 0), collisionPoint))
    require(collisionPoint =~ point2d(1, 0))

  test "collide arc at angle":
    pt = point2d(2, 0);
    require(arc.collides(pt, vector2d(-2, 1), collisionPoint))
    require(collisionPoint =~ point2d(0.8, 0.6))

suite "arc reflections":
  var collisionPoint: Point2d
  var arc: CollidableObject = newCollidableArc(point2d(0, 0), 1, 0, math.PI / 2)

  test "reflect from horizontal axis":
    let vector = arc.reflect(vector2d(-1, -1), point2d(1, 0));
    require (vector =~ vector2d(1, -1))

  test "reflect back":
    let vector = arc.reflect(vector2d(-1, -1), point2d(sqrt(2.0) / 2, sqrt(2.0)/2));
    require vector =~ vector2d(1, 1)

  test "collide arc at angle":
    let vector = arc.reflect(vector2d(-2, -1), point2d(0.8, 0.6));
    require vector =~ vector2d(1.52, 1.64)


suite "2 elements reflection":
  var collisionPoint: Point2d

  test "reflect from moving line":
    let pt = point2d(4, 2)
    let line: CollidableObject = newCollidableLine(point2d(-10, 2), point2d(-6, -2))
    require(line.collides2(pt, vector2d(16, 0), vector2d(-8, -4), collisionPoint))
    require(collisionPoint =~ point2d(0, 0))
    let reflectionSpeed = line.reflect(vector2d(-4, -2), collisionPoint)
    require reflectionSpeed =~ vector2d(2, 4)

  test "reflect from moving circle":
    let pt = point2d(4, 2)
    let ptSpeed = vector2d(-8, -4)
    let arcCenter = point2d(-2, 0)
    let arcSpeed = vector2d(2, 0)
    let arc: CollidableObject = newCollidableArc(arcCenter, 1, 0, math.PI)
    require(arc.collides2(pt, arcSpeed, ptSpeed, collisionPoint))
    require(collisionPoint =~ point2d(0, 0))
    let reflectionSpeed = arc.reflect2(arcSpeed, ptSpeed, collisionPoint)
    echo reflectionSpeed
    require reflectionSpeed =~ vector2d(8, -4) + arcSpeed
