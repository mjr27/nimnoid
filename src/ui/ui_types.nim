import sdl2, basic2d
import tables
type
  # Point2dI = tuple
  #   x, y: cint
  UserInterface * = ref object
    width*: int
    height*: int
    marginTop*: int
    marginLeft*: int
    scale*: float
    window*: sdl2.WindowPtr
    renderer*: sdl2.RendererPtr
    context*: sdl2.GlContextPtr
    textures*: TableRef[string, sdl2.TexturePtr]
  UiCommand * {.pure.} = enum
    Nop
    MoveLeft,
    MoveRight,
    Launch,
    Quit


