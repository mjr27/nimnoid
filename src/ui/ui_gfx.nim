import  basic2d
import sdl2
import ./ui_types


proc drawRect*(self: UserInterface, start: Point2d, size: Vector2d, color: Color) =
  var my_rect = rect(
    self.marginLeft.cint + cint(start.x * self.scale),
    self.marginTop.cint + cint(start.y * self.scale),
    cint(size.x * self.scale),
    cint(size.y * self.scale)
  );
  self.renderer.setDrawColor(color);
  self.renderer.fillRect(my_rect);


proc drawLine*(self: UserInterface, start, finish: Point2d, color: Color) =
  self.renderer.setDrawColor(color);
  self.renderer.drawLine(
    self.marginLeft.cint + cint(start.x * self.scale),
    self.marginTop.cint + cint(start.y * self.scale),
    self.marginLeft.cint + cint(finish.x * self.scale),
    self.marginTop.cint + cint(finish.y * self.scale),
  )


proc drawPixel*(self: UserInterface, position: Point2d, color: Color) =
  self.renderer.setDrawColor(color);
  self.renderer.drawPoint(self.marginLeft.cint + cint(position.x * self.scale), self.marginTop.cint + cint(position.y * self.scale));


proc drawCircle*(self: UserInterface, center: Point2d, radius: float, color: Color) =
  var x0 : cint = self.marginLeft.cint + cint(center.x * self.scale);
  var y0 : cint = self.marginTop.cint + cint(center.y * self.scale);
  var r = cint(radius * self.scale)
  var x: cint = r - 1;
  var y: cint = 0;
  var dx: cint = 1;
  var dy: cint = 1;
  var err: cint = dx - (r shr 2);

  while x >= y:
    self.renderer.drawPoint(x0 + x, y0 + y);
    self.renderer.drawPoint(x0 + y, y0 + x);
    self.renderer.drawPoint(x0 + x, y0 - y);
    self.renderer.drawPoint(x0 + y, y0 - x);
    self.renderer.drawPoint(x0 - x, y0 + y);
    self.renderer.drawPoint(x0 - y, y0 + x);
    self.renderer.drawPoint(x0 - x, y0 - y);
    self.renderer.drawPoint(x0 - y, y0 - x);
    if err <= 0:
      inc(y)
      err += dy
      dy += 2
    if err > 0:
      dec(x)
      dx += 2
      err += (-r shl 1) + dx;

proc drawArc* (self: UserInterface, center: Point2d, radius: float, startAngle: float, endAngle: float, color: Color) =
  var x0 : cint = self.marginLeft.cint + cint(center.x * self.scale);
  var y0 : cint = self.marginTop.cint + cint(center.y * self.scale);
  var r = cint(radius * self.scale)
  var x: cint = r - 1;
  var y: cint = 0;
  var dx: cint = 1;
  var dy: cint = 1;
  var err: cint = dx - (r shr 2);

  let vec0 = vector2d(0, -1)
  proc draw(dx, dy: cint) {.inline.} =
    let angle = angleCW(vector2d(dx.float, dy.float), vec0)
    if angle >= startAngle and angle <= endAngle:
      self.renderer.drawPoint(x0 + dx, y0 + dy);

  while x >= y:
    draw(+x, +y)
    draw(+x, -y)
    draw(-x, +y)
    draw(-x, -y)
    draw(+y, +x)
    draw(+y, -x)
    draw(-y, +x)
    draw(-y, -x)

    if err <= 0:
      inc(y)
      err += dy
      dy += 2
    if err > 0:
      dec(x)
      dx += 2
      err += (-r shl 1) + dx;
