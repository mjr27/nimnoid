import sdl2, sdl2/image, basic2d, math, tables
import ../game/game as gamelib
import ./ui_types, ./ui_gfx
import ./util

proc loadTexture(renderer: RendererPtr, path: string) : sdl2.TexturePtr =
  var str = "assets/sample/" & path
  var surface = image.load(str.cstring)
  if surface.isNil:
    debugEcho "invalid surface " & str
  result = createTextureFromSurface(renderer, surface)
  freeSurface surface;
  discard

proc newUserInterface*(): UserInterface =
  new(result)

proc init*(ui: UserInterface) =
  sdlFailIf(not sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS)): "SDL2 init failed"

  var dm: DisplayMode;
  sdlFailIf(not getDesktopDisplayMode(0, dm)): "Cannot get display mode"

  let window = createWindow("MJR ROGUE", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (dm.w - 100), (dm.h - 100),
  SDL_WINDOW_OPENGL or SDL_WINDOW_RESIZABLE or SDL_WINDOW_SHOWN);
  sdlFailIf window.isNil: "Window could not be created"

  ui.window = window
  ui.renderer = window.createRenderer(index = -1, flags = Renderer_Accelerated or Renderer_PresentVsync)
  ui.textures = newTable[string, TexturePtr]()
  ui.textures["ball"] = ui.renderer.loadTexture "ballBlue.png"
  ui.textures["brick"] = ui.renderer.loadTexture "element_blue_rectangle.png"
  ui.textures["paddle"] = ui.renderer.loadTexture "paddleRed.png"
  # ui.context = window.glCreateContext()


proc quit*(self: UserInterface) =
  sdl2.glDeleteContext(self.context);
  self.window.destroy()
  sdl2.quit()
  system.quit()

var leftDown = false;
var rightDown = false;

proc read*(self: UserInterface): tuple[cmd:UiCommand, ticks: float] =
  var cmd = UiCommand.Nop
  var event = defaultEvent;
  if pollEvent(event):
    case event.kind:
    of QuitEvent: cmd = UiCommand.Quit
    of KeyDown:
      var ev = cast[KeyboardEventPtr](addr event);
      case ev.keysym.sym:
      of sdl2.K_ESCAPE, sdl2.K_q:
        cmd = UiCommand.Quit
      of sdl2.K_LEFT:
        leftDown = true
      of sdl2.K_RIGHT:
        rightDown = true
      of sdl2.K_SPACE:
        cmd = UiCommand.Launch
      else:
        discard
    of KeyUp:
      var ev2 = cast[KeyboardEventPtr](addr event)
      case ev2.keysym.sym:
      of sdl2.K_LEFT:
        leftDown = false
      of sdl2.K_RIGHT:
        rightDown = false
      else:
        discard
    else:
      discard
  if cmd == UiCommand.Nop:
    if leftDown:
      cmd = UICommand.MoveLeft
    elif rightDown:
      cmd = UICommand.MoveRight
  return (cmd:cmd, ticks: getTicks().float / 1000.0)

const MARGIN = 10.0;

proc recalcGamefield(self: UserInterface, size: Vector2d) =
  var viewportRect : Rect;
  self.renderer.getViewport(viewportRect);

  if viewportRect.w == self.width and viewportRect.h == self.height:
    return

  let scale = min((float(viewportRect.w) - MARGIN * 2) / size.x, (float(viewportRect.h) - MARGIN * 2) / size.y)
  let realSize = point2d(size.x * scale, size.y * scale)
  let margin = point2d(viewportRect.w.toFloat - realSize.x, viewportRect.h.toFloat - realSize.y)
  self.width = viewportRect.w
  self.height = viewportRect.h
  self.scale = scale

  self.marginLeft = (margin.x / 2).int
  self.marginTop = (margin.y / 2).int

const backgroundColor: Color = (r:255'u8, g:255'u8, b:0'u8, a:255'u8)
const paddleColor: Color = (r:0'u8, g:0'u8, b:255'u8, a:120'u8)
const colliderColor: Color = (r:255'u8, g:0'u8, b:0'u8, a:255'u8)

proc drawBall(self: UserInterface, coord: Point2d, radius: float) =
  var my_rect = rect(
    self.marginLeft.cint + cint((coord.x - radius) * self.scale),
    self.marginTop.cint + cint((coord.y - radius) * self.scale),
    cint(radius * 2 * self.scale),
    cint(radius * 2 * self.scale),
  );
  self.renderer.copy(self.textures["ball"], nil, addr my_rect)

proc drawBrick(self: UserInterface, coord: Point2d, size: Vector2d) =
  var my_rect = rect(
    self.marginLeft.cint + cint(coord.x * self.scale),
    self.marginTop.cint + cint(coord.y * self.scale),
    cint(size.x * self.scale),
    cint(size.y * self.scale),
  );
  self.renderer.copy(self.textures["brick"], nil, addr my_rect)

proc drawPaddle(self: UserInterface, game: GameState) =
  var my_rect = rect(
    self.marginLeft.cint + cint((game.paddleLeftTop.x - game.ballRadius) * self.scale),
    self.marginTop.cint + cint((game.paddleLeftTop.y) * self.scale),
    cint((game.paddleSize.x + 2 * game.ballRadius) * self.scale),
    cint((game.paddleSize.y) * self.scale),
  );
  self.renderer.copy(self.textures["paddle"], nil, addr my_rect)
  # self.drawRect(game.paddleLeftTop, game.paddleSize, paddleColor)

  # self.drawCircle(
  #   game.paddleCenter - game.paddleSize,
  #   game.paddleSize.y,
  #   paddleColor
  # )
  discard

proc draw*(self: UserInterface, game: GameState) =
  recalcGamefield(self, game.size)
  self.renderer.setDrawColor(110, 132, 174);
  self.renderer.clear();

  self.drawRect(point2d(0, 0), game.size, backgroundColor);

  var coord: Point2d
  if game.ballStuck:
    coord = game.paddleLeftTop  + vector2d(game.paddleSize.x / 4, -game.ballRadius)
  else:
    coord = game.ball.position

  when not defined(release):
    for cld in game.collidableBounds:
      case cld.kind:
      of ColliderKind.Line:
        self.drawLine(cld.p1, cld.p2, colliderColor)
      of ColliderKind.Arc:
        self.drawArc(cld.center, cld.radius, cld.startRad, cld.endRad, colliderColor)
      else:
        discard

  for brick in game.bricks:
    if not brick.broken:
      self.drawBrick(brick.position, brick.size)
      # self.drawRect(brick.position, brick.size, colliderColor)

  self.drawBall(coord, game.ballRadius)
  self.drawPaddle(game)

  self.renderer.present()

export ui_types
