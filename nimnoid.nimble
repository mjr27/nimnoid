# Package

version       = "0.1.0"
author        = "Kyrylo Kobets"
description   = "Arcanoid for nim"
license       = "MIT"
bin           = @["nimnoid"]
srcDir        = "src"

# Dependencies
requires "nim >= 0.17.2"
requires: "sdl2"
requires: "opengl"
requires: "https://github.com/zacharycarter/nuklear-nim"
requires: "glm"
#requires: "assimp"
