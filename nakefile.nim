import nake
const
  MainFile = "src/nimnoid.nim"
  ExeName = "nimnoid.exe"
  BleedingEdgeFeatures = "-d:testFeature1 -d:testFeature2"

task "run", "Build with unproven features":
  shell(nimExe, "c", BleedingEdgeFeatures, "-r", "-o:"&ExeName, MainFile)

task "test", "Build with unproven features":
  shell(nimExe, "c", BleedingEdgeFeatures, "-r", "-o:tests.exe", "src/tests.nim")

task "prod", "Build for production":
  shell(nimExe, "c", BleedingEdgeFeatures, "-r", "-d:release", "-o:"&ExeName, MainFile)
